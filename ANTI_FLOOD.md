# Como utilizar o AntiFlood

```php
<?php

use AutoAction\Utils\AntiFlood\AntiFlood;
use AutoAction\Utils\AntiFlood\Storage\Data;
use AutoAction\Utils\AntiFlood\Storage\Cache;
use AutoAction\Utils\Cache\RedisCustom;

/**
 * Configuração do pacote
 */
$redis = new RedisCustom();
//... config do redis

$af = new AntiFlood(new Data(), new Cache($redis));
$af->setThreshold(10); //* opcional (padrão 12)
$af->setTimeLockInSeconds(300) //*opcional (padrão 5 minutos)
$af->setKey('chave:personalizada');

/**
 * Verificação de Flood
 */
if($af->isFlooding()){
    throw new \Exception('Acesso bloqueado por flood');
}
```