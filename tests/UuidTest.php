<?php

namespace AutoAction\Utils;

use PHPUnit\Framework\TestCase;

/**
 * Class UuidTest
 *
 * @package AutoAction\Utils
 * @date    24/07/2019
 * @author  Lucas Trindade <lucas.silva@autoavaliar.com.br>
 */
class UuidTest extends TestCase
{
    public function testGeneratedUuidIsNotNull()
    {
        $uuid = Uuid::uuid4();
        $this->assertNotNull($uuid);
    }

    public function testGeneratedUuidIsString()
    {
        $uuid = Uuid::uuid4();
        $this->assertTrue(is_string($uuid));
    }
}