<?php

namespace AutoAction\Utils;

use PHPUnit\Framework\TestCase;

/**
 * Class ArrTest
 * @package AutoAction\Utils
 * @date 13/12/2018
 * @author Hugo Santos<hugo.santos@autoavaliar.com.br>
 */
class ArrTest extends TestCase
{
    private $list;

    protected function setUp() {
        $this->list = [
            "payload" => [
                "protocols" => [
                    [
                        "message_id" => 1,
                        "protocol" => "20184",
                        "media" => "image_1",
                        "status" => true
                    ],
                    [
                        "message_id" => 2,
                        "protocol" => "20185",
                        "media" => "image_2",
                        "status" => false
                    ],
                ]
            ]
        ];
    }

    public function testSetDefaultNotFoundKey()
    {
        $originalList = $this->list;

        Arr::setDefaultValue($this->list, "unknown");

        $this->assertEquals($originalList, $this->list);
    }

    public function testSetDefaultValueWithNull()
    {
        Arr::setDefaultValue($this->list, "media");

        $this->assertEquals(null, $this->list['payload']['protocols'][0]['media']);
        $this->assertEquals(null, $this->list['payload']['protocols'][1]['media']);
    }

    public function testSetDefaultValue()
    {
        Arr::setDefaultValue($this->list, "media", 'defaultValue');

        $this->assertEquals('defaultValue', $this->list['payload']['protocols'][0]['media']);
        $this->assertEquals('defaultValue', $this->list['payload']['protocols'][1]['media']);
    }

    public function testSetArrayDefaultValueWithNull()
    {
        Arr::setDefaultValue($this->list, ['protocol', 'status']);

        $this->assertEquals(null, $this->list['payload']['protocols'][0]['protocol']);
        $this->assertEquals(null, $this->list['payload']['protocols'][0]['status']);
        $this->assertEquals(null, $this->list['payload']['protocols'][1]['protocol']);
        $this->assertEquals(null, $this->list['payload']['protocols'][1]['status']);
    }
}