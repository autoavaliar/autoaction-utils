<?php

namespace AutoAction\Utils;

use PHPUnit\Framework\TestCase;

/**
 * Class StrTest
 *
 * @package AutoAction\Utils
 * @date    24/07/2019
 * @author  Lucas Trindade <lucas.silva@autoavaliar.com.br>
 */
class StrTest extends TestCase
{
    public function testReturnedNull()
    {
        $data = Str::stringOrNull(null);
        $this->assertNull($data);
    }

    public function testReturnedString()
    {
        $data = Str::stringOrNull(22);
        $this->assertTrue(is_string($data));
    }

    public function testMultipleReplaces()
    {
        $string = 'abc';
        $replaces = ['a' => 1, 'b' => 2, 'c' => 3];
        $string = Str::multipleReplace($string, $replaces);
        $this->assertEquals($string, '123');
    }
}