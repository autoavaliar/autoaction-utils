<?php

declare(strict_types=1);

namespace Tests\AutoAction\Utils\Vbn;

use AutoAction\Utils\Vbn\Bucket\BucketGeneric;
use AutoAction\Utils\Vbn\Exceptions\VbnException;
use AutoAction\Utils\Vbn\Vbn;
use AutoAction\Utils\Vbn\VbnConfig;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Mockery;
use PHPUnit\Framework\TestCase;

class VbnTest extends TestCase
{

    /**
     * @var VbnConfig
     */
    private $vbnConfig;
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    private $bucket;

    protected function setUp()
    {
        $this->vbnConfig = new VbnConfig('https://google.com', '123456', 'GET');
    }

    /**
     * @throws GuzzleException
     * @throws VbnException
     */
    public function testGetFile()
    {
        $mock = new MockHandler([
                                    new Response(
                                        302,
                                        ['location' => ['mock.jpg']],
                                        ''
                                    )
                                ]);

        $handlerStack = HandlerStack::create($mock);
        $client = new Client(['handler' => $handlerStack]);

        $vbn = new Vbn($this->vbnConfig);
        $vbn->setBucket($this->getBucket());
        $vbn->setClient($client);

        $this->assertEquals('mock.jpg', $vbn->getFile());
    }

    public function testFailedGetFile()
    {
        $this->expectException(GuzzleException::class);
        $mock = new MockHandler([new Response(500)]);

        $handlerStack = HandlerStack::create($mock);
        $client = new Client(['handler' => $handlerStack]);

        $vbn = new Vbn($this->vbnConfig);
        $vbn->setBucket($this->getBucket());
        $vbn->setClient($client);
        $vbn->getFile();
    }

    public function testSetBucket()
    {
        $vbn = new Vbn($this->vbnConfig);
        $vbn->setBucket($this->getBucket());

        $this->assertEquals('bucketFoto', $vbn->getBucket());
    }

    public function testGetFilePath()
    {
        $vbn = new Vbn($this->vbnConfig);
        $vbn->setBucket($this->getBucket());

        $this->assertEquals('foto-fake.jpg', $vbn->getFilePath());
    }

    private function getBucket(): BucketGeneric
    {
        $bucket = new BucketGeneric();
        $bucket->setName('bucketFoto');
        $bucket->setFile('foto-fake.jpg');

        return $bucket;
    }
}
