<?php
declare(strict_types=1);
namespace Tests\AutoAction\Utils\Vbn;

use AutoAction\Utils\Vbn\Exceptions\VbnConfigException;
use AutoAction\Utils\Vbn\VbnConfig;
use PHPUnit\Framework\TestCase;

class VbnConfigTest extends TestCase
{

    /**
     * @dataProvider dataProvider
     */
    public function testConfigVbnException($host, $token, $method)
    {
        $this->expectException(VbnConfigException::class);
        new VbnConfig($host, $token, $method);
    }

    public function testGetHost()
    {
        $vbn = $this->getConfig();

        $this->assertEquals('http://localhost', $vbn->getHost());
    }

    public function testGetToken()
    {
        $vbn = $this->getConfig();
        $this->assertEquals('123456', $vbn->getToken());
    }

    public function testGetMethod()
    {
        $vbn = $this->getConfig();
        $this->assertEquals('GET', $vbn->getMethod());
    }

    public function getConfig(): VbnConfig
    {
        return new VbnConfig('http://localhost', '123456', 'GET');
    }

    public function dataProvider()
    {
        return [
            [
                'invalid_url',
                '123456',
                'GET'
            ],
            [
                'http://localhost',
                '123456',
                'PUT'
            ],
            [
                'http://localhost',
                '123456',
                'DELETE'
            ]
        ];
    }
}