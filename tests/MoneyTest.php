<?php

namespace AutoAction\Utils;

use PHPUnit\Framework\TestCase;

/**
 * Class MoneyTest
 * @package AutoAction\Utils
 * @date 06/09/2019
 * @author Lucas Trindade <lucas.silva@autoavaliar.com.br>
 */
class MoneyTest extends TestCase
{
    public function testRemoveNumberMars()
    {
        $number = 'R$ 10.848,89';
        $this->assertEquals(Money::removeMask($number), '10848.89');
    }

    public function testNumberFormatted()
    {
        $number = '10848.89';
        $this->assertEquals(Money::format($number), '10.848,89');
    }

    public function testEmptyNumberFormatted()
    {
        $number = '0';
        $this->assertEquals(Money::format($number), '0,00');
    }

    public function testValidNumber()
    {
        $number = '10.848,89';
        $this->assertTrue(Money::valid($number));
    }

    public function testInvalidNumber()
    {
        $number = '10.8,4,8,8.9';
        $this->assertFalse(Money::valid($number));
    }

    public function testNumberCents()
    {
        $number = 2.74;
        $this->assertEquals(Money::toCents($number), '274');
    }
}