<?php

declare(strict_types=1);

namespace Tests\AntiFlood;

use AutoAction\Utils\AntiFlood\AntiFlood;
use AutoAction\Utils\AntiFlood\ConfigEnum;
use AutoAction\Utils\AntiFlood\ExceptionKeyNotFound;
use AutoAction\Utils\AntiFlood\Storage\Data;
use AutoAction\Utils\AntiFlood\Storage\StorageMemory;
use PHPUnit\Framework\TestCase;

class AntiFloodRoteExecTest extends TestCase
{
    /**
     * @throws ExceptionKeyNotFound
     */
    public function testNaoBloqueia()
    {
        $limit = 4;
        $flood = true;
        $storage = new StorageMemory();
        $af = new AntiFlood(new Data(), $storage);
        $af->setKey('user-0');
        $af->setThreshold($limit);
        for ($i = 0; $i < $limit - 1; $i++) {
            $flood = $af->isFlooding();
        }
        $storage->delete($af->getKey());
        self::assertFalse($flood);
    }

    /**
     * @throws ExceptionKeyNotFound
     */
    public function testBloqueia()
    {
        $limit = 4;
        $flood = false;
        $storage = new StorageMemory();
        $af = new AntiFlood(new Data(), $storage);
        $af->setKey('user-1');
        $af->setThreshold($limit);
        for ($i = 0; $i < $limit + 2; $i++) {
            $flood = $af->isFlooding();
        }
        $storage->delete($af->getKey());
        self::assertTrue($flood);
    }

    /**
     * @throws ExceptionKeyNotFound
     */
    public function testGetKey_ChaveNaoInformada()
    {
        $this->expectException(ExceptionKeyNotFound::class);
        $storage = new StorageMemory();
        $af = new AntiFlood(new Data(), $storage);
        $af->getKey();
    }

    /**
     * @throws ExceptionKeyNotFound
     */
    public function testGetKey_ChaveGeradaComPrefixo()
    {
        $key = 'user';
        $storage = new StorageMemory();
        $af = new AntiFlood(new Data(), $storage);
        $af->setKey($key);

        $expectedFullKey = $af->getPrefix() . $key;
        self::assertEquals($expectedFullKey, $af->getKey());
    }

    /**
     * @throws ExceptionKeyNotFound
     */
    public function testRegistrosDeAcesso()
    {
        $storage = new StorageMemory();
        $af = new AntiFlood(new Data(), $storage);
        $af->setKey('user-count-0');
        $af->setThreshold(ConfigEnum::THRESHOLD);
        for ($i = 0; $i < ConfigEnum::THRESHOLD; $i++) {
            $af->isFlooding();
        }
        $count = $af->getData()->getRequestCount();
        $storage->delete($af->getKey());
        self::assertEquals(ConfigEnum::THRESHOLD+1, $count);
    }

    //public function testAnalisarRota()
    //{
    //    self::markTestSkipped();
    //
    //    $storage = new StorageMemory();
    //    $af = new AntiFlood(new Data(), $storage);
    //    $af->setKey('user-2');
    //    $af->setThreshold(4);
    //
    //    $isFlood = $af->isFlooding();
    //
    //    $data = $af->getData();
    //
    //    $now = new DateTime();
    //
    //    $dataTime = new DateTime();
    //    $dataTime->setTimestamp((int)$data->getTimestampFirstAccess());
    //
    //    $dataTime2 = new DateTime();
    //    $dataTime2->setTimestamp((int)$data->getTimestampLockedAt());
    //
    //    $response = [
    //        'estaBloqueado' => $isFlood,
    //        'quantidadeDeBloqueios' => $data->getBlockAmount(),
    //        'quantidadeAcessos' => $data->getRequestCount(),
    //        'primeiroAcesso' => $dataTime->format('Y-m-d H:i:s'),
    //        'bloqueadoAte' => $dataTime2->format('Y-m-d H:i:s'),
    //        'agora' => $now->format('Y-m-d H:i:s')
    //    ];
    //
    //    var_dump($response);
    //}
}