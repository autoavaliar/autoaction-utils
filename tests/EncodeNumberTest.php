<?php

namespace Test;

use AutoAction\Utils\EncodeNumber;
use PHPUnit\Framework\TestCase;

class EncodeNumberTest extends TestCase
{
    /**
     * @dataProvider dataConvert
     * @param $number
     * @param $expected
     */
    public function testEncode($number, $expected)
    {
        self::assertEquals($expected, EncodeNumber::encode($number));
    }

    /**
     * @dataProvider dataDecode
     * @param $text
     * @param $expected
     */
    public function testDecode($text, $expected)
    {
        self::assertEquals($expected, EncodeNumber::decode($text));
    }

    public function dataConvert()
    {
        // ['A', 'C', 'f', 'j', 'L', 'd', 'P', 'B', 'g', 'M'];
        return [
            [123, strrev('Cwjz1512')],
            [789, strrev('BgMz6225')],
            [106, strrev('CAPz1123')],
            [1, strrev('Cz1')],
        ];
    }

    public function dataDecode()
    {
        // ['A', 'C', 'f', 'j', 'L', 'd', 'P', 'B', 'g', 'M'];
        return [
            [strrev('Cfjz151'), false],
            [strrev('Cwjz1512'), 123],
            [strrev('BgMz6225'), 789],
            [strrev('CAPz1123'), 106],
            [strrev('capz1123'), false],
            [strrev('Cz1'),1],
            [strrev('iz1'),false],
        ];
    }
}
