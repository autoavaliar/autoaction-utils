<?php

namespace AutoAction\Utils;

use AutoAction\Utils\Plates\Mercosur\Brazil\PlatesConversion;
use PHPUnit\Framework\TestCase;
use Exception;

class PlatesConversionTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testAmountInvalidCharactersExceptionMessage()
    {
        $this->expectExceptionMessage('Amount of invalid characters');
        new PlatesConversion('AAA12345');
    }

    /**
     * @dataProvider providerPlates
     */
    public function testePlates($input, $legacy, $mercosur)
    {
        $plateConversion = new PlatesConversion($input);

        $this->assertEquals($legacy, $plateConversion->getLegacy());
        $this->assertEquals($mercosur, $plateConversion->getMercosur());

        $toArray = [
            'legacy' => $legacy,
            'mercosur' => $mercosur,
        ];

        $this->assertEquals($toArray, $plateConversion->toArray());
    }

    public function providerPlates()
    {
        return [
            'Legacy 0' => ['aaa-0000', 'AAA0000', 'AAA0A00'],
            'Legacy 1' => ['aaa-0100', 'AAA0100', 'AAA0B00'],
            'Legacy 2' => ['aaa-0200', 'AAA0200', 'AAA0C00'],
            'Legacy 3' => ['aaa-0300', 'AAA0300', 'AAA0D00'],
            'Legacy 4' => ['aaa-0400', 'AAA0400', 'AAA0E00'],
            'Legacy 5' => ['aaa-0500', 'AAA0500', 'AAA0F00'],
            'Legacy 6' => ['aaa-0600', 'AAA0600', 'AAA0G00'],
            'Legacy 7' => ['aaa-0700', 'AAA0700', 'AAA0H00'],
            'Legacy 8' => ['aaa-0800', 'AAA0800', 'AAA0I00'],
            'Legacy 9' => ['aaa-0900', 'AAA0900', 'AAA0J00'],

            'Mercosur A' => ['bbb-0A00', 'BBB0000', 'BBB0A00'],
            'Mercosur B' => ['bbb-0B00', 'BBB0100', 'BBB0B00'],
            'Mercosur C' => ['bbb-0C00', 'BBB0200', 'BBB0C00'],
            'Mercosur D' => ['bbb-0D00', 'BBB0300', 'BBB0D00'],
            'Mercosur E' => ['bbb-0E00', 'BBB0400', 'BBB0E00'],
            'Mercosur F' => ['bbb-0F00', 'BBB0500', 'BBB0F00'],
            'Mercosur G' => ['bbb-0G00', 'BBB0600', 'BBB0G00'],
            'Mercosur H' => ['bbb-0H00', 'BBB0700', 'BBB0H00'],
            'Mercosur I' => ['bbb-0I00', 'BBB0800', 'BBB0I00'],
            'Mercosur J' => ['bbb-0J00', 'BBB0900', 'BBB0J00'],
        ];
    }
}
