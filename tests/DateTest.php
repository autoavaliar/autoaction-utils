<?php

namespace AutoAction\Utils;

use PHPUnit\Framework\TestCase;

/**
 * Classe de testes para Date
 * @package AutoAction\Utils
 * @date 16/08/2019
 * @author Murilo Pucci <murilo.pucci@autoavaliar.com.br>
 */
class DateTest extends TestCase
{
    /**
     * @var MockBuilder
     */
    private $mock;

    /**
     * Monta o mock de testes
     * @return void
     */
    protected function setUp()
    {
        $this->mock = $this->getMockBuilder('Date')->setMethods(['nowWithMiliseconds'])->getMock();
    }

    /**
     * Testa se o retorno é um objeto DateTime válido
     * @return void
     */
    public function testNowWithMiliseconds()
    {
        $dateTime = new \DateTime('2019-08-18 15:00:00.0000');
        $this->mock->expects($this->once())
            ->method('nowWithMiliseconds')
            ->will($this->returnValue($dateTime));

        $this->assertSame($dateTime, $this->mock->nowWithMiliseconds());
    }
}