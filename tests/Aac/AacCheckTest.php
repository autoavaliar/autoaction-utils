<?php

declare(strict_types=1);

namespace Aac;

/**
 * Teste unitário para verificação
 *
 * @package Aac
 * @date    11/06/2020 18:59
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */

namespace Aac;

use AutoAction\Utils\Aac\Source\SourceProcess;
use AutoAction\Utils\Aac\AacCheck;
use PHPUnit\Framework\TestCase;

class AacCheckTest extends TestCase
{

    /**
     * @dataProvider provider
     * @param array  $list     Lista de roots permitidas
     * @param string $root     Root a ser verificada
     * @param bool   $expected Resposta esperada
     */
    public function testCheck($list, $root, $expected)
    {
        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        //  Mock do processo de permissões
        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        $source = $this->createMock(SourceProcess::class);
        $source->method('getPermissionList')
            ->willReturn($list);
        $source->method('execute')->willReturn(null);

        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        //  Checagem da permissão
        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        $aac = new AacCheck();
        $aac->addPermissions($source);
        self::assertEquals($expected, $aac->check($root));
    }

    public function provider()
    {
        return [
            [['system:method:read', 'system:method:edit', 'system:method:save'], 'system:method:edit', true],
            [['system:method:read', 'system:method:edit', 'system:method:save'], 'system:method:delete', false],
            [[], 'system:method:edit', false],
        ];
    }
}
