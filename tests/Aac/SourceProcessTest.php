<?php

declare(strict_types=1);

namespace Aac;

/**
 *
 *
 * @package Aac
 * @date    11/06/2020 20:32
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */

namespace Aac;

use AutoAction\Utils\Aac\Source\Database;
use AutoAction\Utils\Aac\Source\SourceProcess;
use AutoAction\Utils\Cache\CacheInterface;
use InvalidArgumentException;
use Phalcon\Db\AdapterInterface;
use PHPUnit\Framework\TestCase;

class SourceProcessTest extends TestCase
{

    /**
     * @dataProvider provider
     * @param array $inputCache    Lista de permissões do cache
     * @param array $inputDatabase Lista de permissões do banco de dados
     */
    public function testGetPermissionList($inputCache, $inputDatabase, $inputCacheExists, $expected, $owner)
    {
        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        //  Conexao com o Redis
        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        $redis = $this->createMock(CacheInterface::class);
        $redis->method('exists')->willReturn($inputCacheExists);
        $redis->method('get')->willReturn($inputCache);

        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        //  Conexao com o banco de dados
        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        $connection = $this->createMock(AdapterInterface::class);
        $connection->method('fetchAll')->with(self::anything())->willReturn($inputDatabase);

        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        //  Fonte de dados do MySql
        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        $source = new Database();
        $source->setInstanceId($owner[0]);
        $source->setPartnerId($owner[1]);
        $source->setConnection($connection);

        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        //  Processa a permissão
        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        $sourceProcess = new SourceProcess();
        $sourceProcess->setRedis($redis);
        $sourceProcess->setSource($source);
        $sourceProcess->execute();

        self::assertEquals($expected, $sourceProcess->getPermissionList());
    }

    public function provider()
    {
        $data = [
            0 => ['root_list'=>'db:method:read'],
            1 => ['root_list'=>'db:method:edit'],
            2 => ['root_list'=>'db:method:save'],
        ];

        $listCache        = ['cache:method:read', 'cache:method:edit', 'cache:method:save'];
        $listDatabase     = $data;
        $expectedCache    = $listCache;
        $expectedDatabase = ['db:method:read', 'db:method:edit', 'db:method:save'];

        $cacheExists    = true;
        $cacheNotExists = false;

        return [
            'dados-do-cache' => [
                $listCache,
                $listDatabase,
                $cacheExists,
                $expectedCache,
                [1,null]
            ],

            'dados-do-banco-de-dados' => [
                $listCache,
                $listDatabase,
                $cacheNotExists,
                $expectedDatabase,
                [1,null]
            ],

            'dados-do-banco-de-dados-parceiro-e-instancia' => [
                $listCache,
                $listDatabase,
                $cacheNotExists,
                $expectedDatabase,
                [1,1]
            ],

            'parceiro-banido' => [
                [],
                ['banned_partner'],
                $cacheNotExists,
                [],
                [null,1]
            ],

            'instancia-banida' => [
                [],
                ['banned_instance'],
                $cacheNotExists,
                [],
                [1,null]
            ],
        ];
    }

}
