# Projeto: AutoAction

Este projeto tem como objetivo facilitar a integração com a HG API e Ecossistema.

### Dependências

- Redis
- PHP 7.0 ou superior
- Ser um parceiro Auto Avaliar / Auto Action

### Como utilizar a conversão de placas para o padrão Mercosul e legado?

```php
<?php

use AutoAction\Utils\Plates\Mercosur\Brazil\PlatesConversion;

$plateConversion = new PlatesConversion('PLACA');

//Retorno da conversão com o padrão legado
$plateConversion->getLegacy();

//Retorno da conversão com o padrão Mercosul
$plateConversion->getMercosur();

//retorno da conversão como array
$plateConversion->toArray();

```

---

##### Exemplo de conversão legado para Mercosul.

```php
<?php

use AutoAction\Utils\Plates\Mercosur\Brazil\PlatesConversion;

//A placa pode ser formato no formato legado ou Mercosul
$plateConversion = new PlatesConversion('AAA-0000');
//AAA0000
$plateConversion->getLegacy(); 
//AAA0A00
$plateConversion->getMercosur();
/**
 *   [
 *     'legacy' => $legacy,
 *     'mercosur' => $mercosur,
 *   ]
*/
$plateConversion->toArray(); 

```

### Informações Docker

 Adicione no seu arquivo de hosts
 
 ```127.0.0.1   web.local```
 
 Normalmente este arquivo fica em: `/etc/hosts`
 
 Para iniciar o container rode o comando:
 
 `docker-compose up -d`
 
 ### Acessando a URL do projeto
 
 Adicione no seu navegador ou postman `http://web.local:8190`
 
 ### Configurando o Xdebug no PhpStorm
 
 Vá no menu de configuração `File > Settings... > Languagens & Frameworks > PHP > Debug`
 
 Na seção `Xdebug` no campo `Debug port:` coloque a porta `108190`
 
 Também é preciso mapear as pastas do container
 
 Vá no menu de configuração `File > Settings... > Languagens & Frameworks > PHP > Servers`
 
 ### Outros comandos Docker
 
 Inicia Docker: `docker-compose up -d`
 
 Encerrar Docker: `docker-compose down`
 
 Levantar o docker com rebuild do container: `docker-compose up -d --build`
 
 Entrar no shell do container: `docker exec -it autoaction-utils-phpfpm /bin/sh`
 
 Verificar os logs de acesso do nginx: `docker logs -tf autoaction-utils-web`
 
 