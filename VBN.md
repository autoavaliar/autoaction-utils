# Como configurar o VBN no sistema

VBN = Virtual Bucket Network

``` php
$host = 'https://'; 
$token = "eyJ0eX_token"; // token partner hg
try {
$config = new VbnConfig($host, $token);
} catch (\AutoAction\Utils\Vbn\Exceptions\VbnConfigException $e) {
    print $e->getMessage();
    exit();
}

$bucket = new Bucket;
$vbn = new Vbn($config);
$vbn->setBucket($bucket);
$vbn->setClient(new \GuzzleHttp\Client());

try {
    print "<img src='".$vbn->getFile()."'>";
} catch (\AutoAction\Utils\Vbn\Exceptions\VbnException | GuzzleHttp\Exception\GuzzleException $e) {
     print $e->getMessage();
}
```