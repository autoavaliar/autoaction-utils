# Como configurar o AAC no sistema

AAC = API Account Control

```$php
<?php
use AutoAction\Utils\Cache\RedisCustom;
use AutoAction\Utils\Aac\Source\SourceProcess;
use AutoAction\Utils\Aac\Source\Database;
use AutoAction\Utils\Aac\AacCheck;

$redis = new RedisCustom();
....

$source = new Database();
$source->setInstanceId(1);
$source->setPartnerId(null);
$source->setConnection($this->di->getDb());

$sourceProcess = new SourceProcess();
$sourceProcess->setRedis($redis);
$sourceProcess->setSource($source);

$aac = new AacCheck();
$aac->addPermissions($sourceProcess);
$aac->check('system:method:edit');

```