<?php

declare(strict_types=1);

namespace AutoAction\Utils;

/**
 * IP
 *
 * @package AutoAction\Utils
 * @date    24/10/2018
 *
 * @author  Lucas Trindade <lucas.silva@autoavaliar.com.br>
 */
class IP
{
    /**
     * Consulta do ip do usuário
     * @return string
     */
    public static function current() : string
    {
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            return $_SERVER['HTTP_CLIENT_IP'];
        }

        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        return $_SERVER['REMOTE_ADDR'] ?? '';
    }
}