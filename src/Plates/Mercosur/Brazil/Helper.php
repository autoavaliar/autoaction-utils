<?php

declare(strict_types=1);

namespace AutoAction\Utils\Plates\Mercosur\Brazil;

/**
 * Class Helper
 *
 * @package AutoAction\Utils\Plates\Mercosur\Brazil
 * @author Reginaldo Castardo <reginaldo.castardo@autoavaliar.com.br>
 * @date 01/06/2020
 */
class Helper
{
    const MAXIMUM_PLATE_LENGTH = 7;
    const POSITION_DIGIT_PLATE = 4;
    const OPTIONS_PATTERN = [
        0 => 'A',
        1 => 'B',
        2 => 'C',
        3 => 'D',
        4 => 'E',
        5 => 'F',
        6 => 'G',
        7 => 'H',
        8 => 'I',
        9 => 'J',
    ];

    protected function removeHyphen(string $plate): string
    {
        return strtoupper(str_replace('-', '', $plate));
    }

    protected function validateLengthPlate(string $plate): bool
    {
        return strlen($plate) === self::MAXIMUM_PLATE_LENGTH;
    }

    protected function getDigitPlate(string $plate): string
    {
        return $plate[self::POSITION_DIGIT_PLATE];
    }

    /**
     * @param $plate
     * @param string $replace Plate digit to be used for replacement
     * @param bool $isNumeric Used to return the value contained in the array
     * @return string
     */
    protected function replaceDigit(string $plate, string $replace, bool $isNumeric = false): string
    {
        $split = str_split($plate);
        $split[self::POSITION_DIGIT_PLATE] = (
            $isNumeric
                ? self::OPTIONS_PATTERN[$replace]
                : array_flip(self::OPTIONS_PATTERN)[$replace]
        );

        return vsprintf('%s%s%s%s%s%s%s', $split);
    }
}
