<?php

declare(strict_types=1);

namespace AutoAction\Utils\Plates\Mercosur\Brazil;

use Exception;

/**
 * Class PlatesConversion
 *
 * @package AutoAction\Utils\Plates\Mercosur\Brazil
 * @author Reginaldo Castardo <reginaldo.castardo@autoavaliar.com.br>
 * @date 02/06/2020
 * @final
 */
final class PlatesConversion extends Helper
{
    private $plate;
    private $legacy;
    private $mercosur;

    /**
     * @param string $plate
     * @throws Exception
     */
    public function __construct($plate)
    {
        $this->plate = (string)$plate;
        $this->execute();
    }

    /**
     * @throws Exception
     */
    private function execute()
    {
        $plate = $this->removeHyphen($this->plate);

        if (!$this->validateLengthPlate($plate)) {
            throw new Exception('Amount of invalid characters');
        }

        $digit = $this->getDigitPlate($plate);

        if (is_numeric($digit)) {
            $this->legacy = $plate;
            $this->mercosur = $this->replaceDigit($plate, (string)$digit, true);
        } else {
            $this->legacy = $this->replaceDigit($plate, (string)$digit);
            $this->mercosur = $plate;
        }
    }

    public function getLegacy(): string
    {
        return $this->legacy;
    }

    public function getMercosur(): string
    {
        return $this->mercosur;
    }

    public function toArray(): array
    {
        return [
            'legacy' => $this->getLegacy(),
            'mercosur' => $this->getMercosur(),
        ];
    }
}
