<?php

namespace AutoAction\Utils;

/**
 * Class Money
 * @package AutoAction\Utils
 * @date    06/09/2019
 * @author  Lucas Trindade <lucas.silva@autoavaliar.com.br>
 */
class Money
{
    /**
     * Retorno a string numérica formatada para valor com decimais com ponto para o banco de dados.
     * @param string $value
     * @return string
     */
    public static function removeMask(string $value): string
    {
        $value = str_replace("R$ ", '', $value);
        $value = str_replace(".", "", $value);
        $value = str_replace(",", ".", $value);
        return $value;
    }

    /**
     * Retorno a string numérica formatada para valor com decimais com virgula
     * @param $value
     * @param int $decimals
     * @return string
     */
    public static function format($value, $decimals = 2): string
    {
        if(empty($value)) {
            return '0,00';
        }

        $value = number_format($value, $decimals, ',', '.');
        return $value;
    }

    /**
     * Valida se o valor é numérico
     * @param $value
     * @return boolean
     */
    public static function valid($value) : bool
    {
        $value = (string) $value;
        $role = "/^[0-9]{1,3}([.]([0-9]{3}))*[,]([.]{0})[0-9]{0,2}$/";

        return preg_match($role, $value) || is_numeric($value);
    }

    /**
     * Conversão de um valor para centavos
     * @param float $value
     * @return int
     */
    public static function toCents(float $value) : int
    {
        $integer = (int)$value;
        $cents = ($value - $integer) * 100;
        return (int)(($integer * 100) + $cents);
    }
}