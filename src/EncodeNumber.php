<?php

declare(strict_types=1);

namespace AutoAction\Utils;

/**
 * Codificação de numero
 *
 * @package AutoAction\Utils
 * @date   23/03/2020 18:34
 *
 * @author Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class EncodeNumber
{
    private static $mapLetters = ['A', 'C', 'w', 'j', 'L', 'd', 'P', 'B', 'g', 'M'];

    /**
     * Informe o numero que deseja converter para string
     * @param int $number Numero a converter
     * @return string
     */
    public static function encode(int $number)
    {
        $partials = str_split((string)$number, 1);

        $result = '';
        foreach ($partials as $key => $value) {
            $result .= self::$mapLetters[$value];
        }

        $pow = self::getVerifyingDigit($number);

        return strrev(($result . 'z' . $pow));
    }

    /**
     * Informe a string que deseja decodificar
     * @param string $string String codificada
     * @return bool|int
     */
    public static function decode(string $string)
    {
        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        //  input
        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        $normalize = strrev($string);
        $detach = explode('z', $normalize);

        if (!isset($detach[0]) || !isset($detach[1])) {
            return false;
        }

        $letter = (string)$detach[0];
        $inputDigit = $detach[1];

        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        //  decodifica letras em numeros
        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        $partials = str_split($letter, 1);

        $number = '';

        foreach ($partials as $value) {
            if (!in_array($value, self::$mapLetters)) {
                return false;
            }

            $key = array_search($value, self::$mapLetters);
            $number .= $key;
        }

        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        //  validação
        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        $digit = self::getVerifyingDigit($number);

        if ($digit != $inputDigit) {
            return false;
        }

        return (int)$number;
    }

    /**
     * Gerar o dígito verificador
     * @param string $value numero de origem
     * @return false|string
     */
    private static function getVerifyingDigit($value)
    {
        $pow = (string)pow($value, 2);
        if (strlen($pow) > 4) {
            $pow = substr($pow, 0, 4);
        }
        return $pow;
    }
}