<?php

namespace AutoAction\Utils;

/**
 * Class Arr
 * @package AutoAction\Utils
 * @date    12/12/2018
 * @author  Hugo Leonardo <hugo.santos@autoavaliar.com.br>
 */
class Arr
{
    public static function setDefaultValue(&$array, $index, $defaultValue = null)
    {
        if(is_array($index)){
            foreach($index as $value) {
                if(isset($array[$value])){
                    $array[$value] = $defaultValue;
                }
            }
        } else {
            if(isset($array[$index])){
                $array[$index] = $defaultValue;
            }
        }

        if(is_array($array)) {
            foreach ($array as &$value) {
                if (is_array($value)) {
                    self::setDefaultValue($value, $index, $defaultValue);
                }
            }
        }
    }
}