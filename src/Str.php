<?php

declare(strict_types=1);

namespace AutoAction\Utils;

/**
 * Str
 *
 * @package AutoAction\Utils
 * @date    16/10/2018
 *
 * @author  Lucas Trindade <lucas.silva@autoavaliar.com.br>
 */
class Str
{
    /**
     * Inserção de conteúdo à esquerda da string
     *
     * @param string $string
     * @param string $pad
     * @param int $length
     * @return string
     */
    public static function lpad(string $string, string $pad, int $length): string
    {
        return str_pad($string, $length, $pad, STR_PAD_LEFT);
    }

    /**
     * Remove todos os espaços em branco de uma string
     *
     * @param string $string
     * @return string
     */
    public static function removeWhiteSpaces(string $string): string
    {
        return str_replace(' ', '', $string);
    }

    /**
     * Mantém apenas caracteres alfanuméricos na string
     *
     * @param string $string
     * @return string
     */
    public static function alphaNum(string $string): string
    {
        return preg_replace('/[^a-zA-Z0-9]+/', '', $string);
    }

    /**
     * Verificação se string está serializada
     *
     * @param string $data
     * @param bool $strict
     * @return bool
     */
    public static function isSerialized(string $data, bool $strict = true): bool
    {
        // if it isn't a string, it isn't serialized.
        if (!is_string($data)) {
            return false;
        }
        $data = trim($data);
        if ('N;' == $data) {
            return true;
        }
        if (strlen($data) < 4) {
            return false;
        }
        if (':' !== $data[1]) {
            return false;
        }
        if ($strict) {
            $lastc = substr($data, -1);
            if (';' !== $lastc && '}' !== $lastc) {
                return false;
            }
        } else {
            $semicolon = strpos($data, ';');
            $brace     = strpos($data, '}');
            // Either ; or } must exist.
            if (false === $semicolon && false === $brace)
                return false;
            // But neither must be in the first X characters.
            if (false !== $semicolon && $semicolon < 3)
                return false;
            if (false !== $brace && $brace < 4)
                return false;
        }
        $token = $data[0];
        switch ($token) {
            case 's' :
                if ($strict) {
                    if ('"' !== substr($data, -2, 1)) {
                        return false;
                    }
                } elseif (false === strpos($data, '"')) {
                    return false;
                }
            // or else fall through
            case 'a' :
            case 'O' :
                return (bool)preg_match("/^{$token}:[0-9]+:/s", $data);
            case 'b' :
            case 'i' :
            case 'd' :
                $end = $strict ? '$' : '';
                return (bool)preg_match("/^{$token}:[0-9.E-]+;$end/", $data);
        }
        return false;
    }

    /**
     * Desserealiza uma string
     *
     * @param string|null $string
     * @return mixed|string
     */
    public static function unserialize(string $string = null)
    {
        if(empty($string)){
            return $string;
        }

        if(!self::isSerialized($string)){
            return $string;
        }

        return unserialize($string);
    }

    /**
     * Devolve uma string randômica
     *
     * @param string $prefix
     * @return string
     * @throws \Exception
     */
    public static function random(string $prefix = ''): string
    {
        $random = bin2hex(random_bytes(15));
        $date = \DateTime::createFromFormat('U.u', microtime(TRUE));
        return "{$prefix}{$date->format('Y-m-d\TH.i.s.u')}-{$random}";
    }

    /**
     * Devolve uma variável como string caso tenha dados, ou como null se estiver vazia
     *
     * @param int|string|null $data
     * @return string|null
     */
    public static function stringOrNull($data)
    {
        if(is_null($data)){
            return null;
        }

        return (string)$data;
    }

    /**
     * Efetua substituição de valores em uma string
     * 
     * * Example:
     * ```
     * $string = 'abc';
     * $replaces = ['a' => 1, 'b' => 2, 'c' => 3];
     * $string = Str::multipleReplace($string, $replaces);
     * // $string = '123'
     * ```
     * 
     * @param string $string valor que terá substituições
     * @param array $replaces array associativo com as substituições, onde chave é o search, e valor é o replace
     * @return string
     */
    public static function multipleReplace(string $string, array $replaces): string
    {
        foreach ($replaces as $search => $replace) {
            $string = str_replace($search, $replace, $string);
        }

        return $string;
    }
}