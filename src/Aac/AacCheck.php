<?php

declare(strict_types=1);

namespace AutoAction\Utils\Aac;

use AutoAction\Utils\Aac\Source\SourceProcess;

/**
 * Verifica o AAC AutoAction
 *
 * @package AutoAction\Utils\Aac
 * @date    11/06/2020 18:57
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class AacCheck
{
    /**
     * @var SourceProcess
     */
    private $aacCache;

    public function addPermissions(SourceProcess $aacCache)
    {
        $this->aacCache = $aacCache;
        $this->aacCache->execute();
    }

    public function check(string $root)
    {
        if (in_array(trim($root), $this->aacCache->getPermissionList())) {
            return true;
        } else {
            return false;
        }
    }
}