<?php

declare(strict_types=1);

namespace AutoAction\Utils\Aac\Source;

use Exception;
use PDO;
use Phalcon\Db\AdapterInterface;

/**
 * Busca a lista de permissões do banco de dados
 *
 * @package AutoAction\Utils\Aac\Source
 * @date    12/06/2020 09:15
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class Database extends SourceAbstract
{
    /**
     * Instancia da conexão com o banco de dados (Phalcon PHP)
     * @var AdapterInterface
     */
    private $connection;

    /**
     * Lista de permissões
     * @var array
     */
    private $permissionList = [];

    /**
     * Informe uma conexão válida do PDO via Phalcon PHP
     * @param AdapterInterface $connection Conexão PDO do Phalcon PHP
     */
    public function setConnection(AdapterInterface $connection)
    {
        $this->connection = $connection;
    }

    private function getProcedurePartner()
    {
        return 'call hgapi.listAACRootsByPartner(:id)';
    }

    private function getProcedureInstance()
    {
        return 'call ego.listAACRootsByInstance(:id)';
    }

    private function getDataFromPartner()
    {
        $data = $this->connection->fetchAll(
            $this->getProcedurePartner(),
            PDO::FETCH_ASSOC,
            ['id' => $this->getPartnerId()]
        );

        if(is_array($data)){
            $data = array_column($data,'root_list');
        }

        if (!$data || $data[0] == 'banned_partner') {
            return [];
        }

        return $data;
    }

    private function getDataFromInstance()
    {
        $data = $this->connection->fetchAll(
            $this->getProcedureInstance(),
            PDO::FETCH_ASSOC,
            ['id' => $this->getInstanceId()]
        );

        if(is_array($data)){
            $data = array_column($data,'root_list');
        }

        if (!$data || $data[0] == 'banned_instance') {
            return [];
        }

        return $data;
    }

    public function execute()
    {
        $this->validateInput();

        try {
            if ($this->getInstanceId()) {
                $this->permissionList = array_merge($this->permissionList, $this->getDataFromInstance());
            }

            if ($this->getPartnerId()) {
                $this->permissionList = array_merge($this->permissionList, $this->getDataFromPartner());
            }

            $this->permissionList = array_unique($this->permissionList);

        } catch (Exception $e) {
            $this->permissionList = [];
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getPermissionList()
    {
        return $this->permissionList;
    }
}