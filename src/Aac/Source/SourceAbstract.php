<?php

declare(strict_types=1);

namespace AutoAction\Utils\Aac\Source;

use InvalidArgumentException;

/**
 *
 *
 * @package AutoAction\Utils\Aac\Source
 * @date    12/06/2020 10:07
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
abstract class SourceAbstract
{
    private $partnerId;
    private $instanceId;

    public function setPartnerId($partnerId)
    {
        $this->partnerId = (int)$partnerId;
    }

    public function setInstanceId($instanceId)
    {
        $this->instanceId = (int)$instanceId;
    }

    public function getPartnerId()
    {
        return $this->partnerId;
    }

    public function getInstanceId()
    {
        return $this->instanceId;
    }

    public function validateInput()
    {
        if (is_null($this->instanceId) && is_null($this->partnerId)) {
            throw new InvalidArgumentException('InstanceId or PartnerId not defined!', E_ERROR);
        }
    }

    public abstract function execute();

    public abstract function getPermissionList();
}