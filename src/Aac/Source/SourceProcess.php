<?php

declare(strict_types=1);

namespace AutoAction\Utils\Aac\Source;

use AutoAction\Utils\Cache\CacheInterface;

/**
 * Cache da lista de permissões
 *
 * @package AutoAction\Utils\Aac
 * @date    11/06/2020 19:10
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class SourceProcess
{
    private $redis;
    private $cacheTtl = (60 * 5);

    /**
     * Lista de permissões de uma instancia ou parceiro
     * @var array
     */
    private $permissionList=[];


    /**
     * @var SourceAbstract
     */
    private $source;

    public function setSource(SourceAbstract $source)
    {
        $this->source = $source;
    }

    /**
     * Informe uma conexão ativa do redis
     * @param CacheInterface $redis Instancia do Redis
     */
    public function setRedis(CacheInterface $redis)
    {
        $this->redis = $redis;
    }

    public function getKey()
    {
        return sprintf(
            'aac:partner:%s:instance:%s',
            (int)$this->source->getPartnerId(),
            (int)$this->source->getInstanceId()
        );
    }

    public function execute()
    {
        if ($this->redis->exists($this->getKey())) {
            $this->permissionList = $this->redis->get($this->getKey());
            return;
        }

        $this->source->execute();

        $this->permissionList = $this->source->getPermissionList();
        $this->redis->save($this->getKey(), $this->permissionList, $this->cacheTtl);
    }

    public function cacheDelete()
    {
        return $this->redis->delete($this->getKey());
    }

    public function getPermissionList(): array
    {
        return $this->permissionList;
    }

}