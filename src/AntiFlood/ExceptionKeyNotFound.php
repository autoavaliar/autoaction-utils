<?php

declare(strict_types=1);

namespace AutoAction\Utils\AntiFlood;

use Exception;

/**
 * Chave não encontrada
 *
 * @package AutoAction\Utils\AntiFlood
 * @date    09/08/2021 13:59
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class ExceptionKeyNotFound extends Exception
{

}