<?php

declare(strict_types=1);

namespace AutoAction\Utils\AntiFlood;

use Exception;

/**
 * Exception padrão do Anti Flood
 *
 * @package AutoAction\Utils\AntiFlood
 * @date    12/08/2021 11:04
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class AntiFloodException extends Exception
{

}