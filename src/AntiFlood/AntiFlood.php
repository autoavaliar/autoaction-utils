<?php

declare(strict_types=1);

namespace AutoAction\Utils\AntiFlood;

use AutoAction\Utils\AntiFlood\Storage\Data;
use AutoAction\Utils\AntiFlood\Storage\StorageInterface;

/**
 * Sistema AntiFlood
 *
 * @package AutoAction\Utils\AntiFlood
 * @date    05/08/2021 11:55
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class AntiFlood
{
    /** @var float Timestamp inicial */
    private $startTimestamp = 1183802827.000;

    /** @var int Intervalo de bloqueio em segundos */
    private $timeLockInSeconds = ConfigEnum::FIVE_MINUTES_IN_SECONDS;

    /** @var int Quantidade de acessos por segundo */
    private static $threshold = ConfigEnum::THRESHOLD;

    /** @var string */
    private $key;

    /** @var string */
    private $prefix = 'aa-anti-flood_';

    /** @var int */
    private $cacheDurationInSeconds = ConfigEnum::HALF_DAY_IN_SECONDS;

    /** @var Data Dados de acesso */
    private $data;

    /** @var StorageInterface Armazenamento dos dados de acesso */
    private $storage;

    public static $isBlock;
    public static $blockAmount;

    public function __construct(Data $data, StorageInterface $storage)
    {
        $this->setData($data);
        $this->setStorage($storage);
    }

    public function getTimeLockInSeconds(): int
    {
        return $this->timeLockInSeconds;
    }

    /**
     * @param int $timeLockInSeconds Tempo de bloqueio sem segundos *opcional (padrão 5 minutos)
     *
     * @return $this
     */
    public function setTimeLockInSeconds(int $timeLockInSeconds): AntiFlood
    {
        $this->timeLockInSeconds = $timeLockInSeconds;
        return $this;
    }

    /**
     * @throws ExceptionKeyNotFound
     */
    public function getData(): Data
    {
        if ($this->storage->exists($this->getKey())) {
            return $this->storage->get($this->getKey());
        }
        return $this->data;
    }

    public function setData(Data $data): AntiFlood
    {
        $this->data = $data;
        return $this;
    }

    public function getCacheDurationInSeconds(): int
    {
        return $this->cacheDurationInSeconds;
    }

    public function setCacheDurationInSeconds(int $cacheDurationInSeconds): AntiFlood
    {
        $this->cacheDurationInSeconds = $cacheDurationInSeconds;
        return $this;
    }

    public static function getThreshold(): int
    {
        return self::$threshold;
    }

    /**
     * @param int $threshold Quantidade de acessos por segundo *opcional
     */
    public static function setThreshold(int $threshold)
    {
        self::$threshold = $threshold;
    }

    public function getStorage(): StorageInterface
    {
        return $this->storage;
    }

    public function setStorage(StorageInterface $storage): AntiFlood
    {
        $this->storage = $storage;
        return $this;
    }

    public function getPrefix(): string
    {
        return $this->prefix;
    }

    public function setKey(string $key): AntiFlood
    {
        $this->key = $this->getPrefix() . trim($key);
        return $this;
    }

    /**
     * @throws ExceptionKeyNotFound
     */
    public function getKey(): string
    {
        if (is_null($this->key)) {
            throw new ExceptionKeyNotFound('Chave não informada!');
        }

        return $this->key;
    }

    /**
     * @throws ExceptionKeyNotFound
     */
    public function getTimestampForLock(): float
    {
        return $this->getBlockingIntervalInSeconds() + microtime(true);
    }

    /**
     * @throws ExceptionKeyNotFound
     */
    public function getBlockingIntervalInSeconds(): float
    {
        if ($this->getData()->getBlockAmount() <= 1) {
            return (float)$this->getTimeLockInSeconds();
        }
        $timeAdditional = ($this->getData()->getBlockAmount() * $this->getTimeLockInSeconds());
        return (float)($this->getTimeLockInSeconds() + $timeAdditional);
    }

    /**
     * @throws ExceptionKeyNotFound
     */
    public function getTimestampLockedUntil(): float
    {
        return $this->getData()->getTimestampFirstAccess() + $this->getBlockingIntervalInSeconds();
    }

    /**
     * @throws ExceptionKeyNotFound
     */
    public function checkIfBlocked(): bool
    {
        $blockForTimestamp = (microtime(true) < $this->getData()->getTimestampLockedAt());
        if ($blockForTimestamp) {
            return true;
        }
        return false;
    }

    /**
     * @throws ExceptionKeyNotFound
     */
    public function getSessionInfo(): Data
    {
        if ($this->storage->exists($this->getKey())) {
            return $this->storage->get($this->getKey());
        }
        return $this->getData()
            ->setTimestampFirstAccess(microtime(true))
            ->setTimestampLockedAt($this->startTimestamp)
            ->setRequestCount(1);
    }

    /**
     * @throws ExceptionKeyNotFound
     */
    public function isFlooding(): bool
    {
        $session = $this->getSessionInfo();
        /**
         * Cenário 1: Registra o bloqueio do usuário
         */
        $this->registerLock($session);
        /**
         * Cenário 2: Registra o acesso do usuário
         */
        $this->registerAccess($session);
        /**
         * Cenário 3: Registra o desbloqueio do usuário ou inicializa
         */
        $this->reset($session);
        $this->saveData($session);

        self::$isBlock = $this->checkIfBlocked();
        self::$blockAmount = $session->getBlockAmount();
        return self::$isBlock;
    }

    /**
     * @throws ExceptionKeyNotFound
     */
    private function reset(Data $data)
    {
        if (!$this->checkIfBlocked() && (microtime(true) > $data->getTimestampFirstAccess() + 60)) {
            $data->setTimestampFirstAccess(microtime(true));
            $data->setTimestampLockedAt($this->startTimestamp);
            $data->setRequestCount(1);
        }
    }

    /**
     * @throws ExceptionKeyNotFound
     */
    private function registerAccess(Data $data)
    {
        if (!$this->checkIfBlocked() && $data->getTimestampLockedAt() < microtime(true)) {
            $data->registerRequestCount();
        }
    }

    /**
     * @throws ExceptionKeyNotFound
     */
    private function registerLock(Data $data)
    {
        $blockForThreshold = ($this->getData()->getRequestCount() > self::getThreshold());
        $blockForTime = ($data->getTimestampLockedAt() <= microtime(true));
        if (($this->checkIfBlocked() || $blockForThreshold) && $blockForTime) {
            $data->setTimestampLockedAt($this->getTimestampForLock());
            $data->setBlockAmount($data->getBlockAmount() + 1);
            //$data->setRequestCount(1);
        }
    }

    /**
     * @throws ExceptionKeyNotFound
     */
    private function saveData(Data $data)
    {
        $this->storage->save($this->getKey(), $data, $this->getCacheDurationInSeconds());
    }

    /**
     * @throws ExceptionKeyNotFound
     */
    private function register(Data $data, $isBlock)
    {
        /**
         * Cenário 1: Registra o bloqueio do usuário
         */
        if ($isBlock && $data->getTimestampLockedAt() <= microtime(true)) {
            $data->setTimestampLockedAt($this->getTimestampForLock());
            $data->setBlockAmount($data->getBlockAmount() + 1);
            $data->setRequestCount(1);
        }
        $this->registerLock($data);

        /**
         * Cenário 2: Registra o acesso do usuário
         */
        //if (!$isBlock && $data->getTimestampLockedAt() < microtime(true)) {
        //    $data->registerRequestCount();
        //}
        $this->registerAccess($data);

        /**
         * Cenário 3: Registra o desbloqueio do usuário ou inicializa
         */
        $this->reset($data);
        //if (!$isBlock && (microtime(true) > $data->getTimestampFirstAccess() + 60)) {
        //    $this->reset($data);
        //    return $this->saveData($data);
        //}

        $this->saveData($data);
    }


}