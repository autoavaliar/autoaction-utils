<?php

declare(strict_types=1);

namespace AutoAction\Utils\AntiFlood;

/**
 * Configuração padrão
 *
 * @package AutoAction\Utils\AntiFlood
 * @date    09/08/2021 16:17
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class ConfigEnum
{
    const THRESHOLD = 12;
    const FIVE_MINUTES_IN_SECONDS = 300;
    const HALF_DAY_IN_SECONDS = 43200;
}