<?php

declare(strict_types=1);

namespace AutoAction\Utils\AntiFlood\Storage;

/**
 * Dados de acesso
 *
 * @package AutoAction\Utils\AntiFlood
 * @date    05/08/2021 11:30
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class Data
{
    /** @var float Último acesso em x timestamp */
    private $timestampFirstAccess = 0.0;

    /** @var float Bloqueado em x timestamp */
    private $timestampLockedAt = 0.0;

    /** @var int Quantidade de acessos */
    private $requestCount = 0;

    private $blockAmount = 0;

    public function getTimestampFirstAccess(): float
    {
        return $this->timestampFirstAccess;
    }

    public function setTimestampFirstAccess(float $timestampFirstAccess): Data
    {
        $this->timestampFirstAccess = $timestampFirstAccess;
        return $this;
    }

    public function getTimestampLockedAt(): float
    {
        return $this->timestampLockedAt;
    }

    public function setTimestampLockedAt(float $timestampLockedAt): Data
    {
        $this->timestampLockedAt = $timestampLockedAt;
        return $this;
    }

    public function getRequestCount(): int
    {
        return $this->requestCount;
    }

    public function setRequestCount(int $requestCount): Data
    {
        $this->requestCount = $requestCount;
        return $this;
    }

    public function registerRequestCount(): Data
    {
        $this->requestCount++;
        return $this;
    }

    public function getBlockAmount(): int
    {
        return $this->blockAmount;
    }

    public function setBlockAmount(int $blockAmount): Data
    {
        $this->blockAmount = $blockAmount;
        return $this;
    }

}