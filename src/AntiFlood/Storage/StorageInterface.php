<?php

declare(strict_types=1);

namespace AutoAction\Utils\AntiFlood\Storage;

/**
 * “Interface” que define a assinatura dos objetos Storage
 *
 * @package AutoAction\Utils\AntiFlood
 * @date    05/08/2021 11:38
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
interface StorageInterface
{
    public function exists(string $key);

    public function get(string $key);

    public function delete(string $key);

    public function save(string $key, $value, int $lifetime);
}