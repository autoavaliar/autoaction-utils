<?php

declare(strict_types=1);

namespace AutoAction\Utils\AntiFlood\Storage;

use AutoAction\Utils\Cache\CacheInterface;

/**
 * Storage Redis
 *
 * @package AutoAction\Utils\AntiFlood\Storage
 * @date    05/08/2021 11:42
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class Cache implements StorageInterface
{
    /** @var CacheInterface */
    private $cache;

    public function __construct(CacheInterface $cache)
    {
        $this->cache = $cache;
    }

    public function exists(string $key): int
    {
        return (int)$this->cache->exists($key);
    }

    public function get(string $key)
    {
        return $this->cache->get($key);
    }

    public function delete(string $key): int
    {
        return (int)$this->cache->delete($key);
    }

    public function save(string $key, $value, int $lifetime): bool
    {
        return (bool)$this->cache->save($key, $value, $lifetime);
    }
}