<?php

declare(strict_types=1);

namespace AutoAction\Utils\AntiFlood\Storage;

class StorageMemory implements StorageInterface
{
    public function file($key)
    {
        return dirname(__FILE__) . '/' . $key . '.txt';
    }

    public function exists(string $key): bool
    {
        if (!file_exists($this->file($key))) {
            return false;
        }

        return true;
    }

    public function get(string $key)
    {
        $arquivo = file_get_contents($this->file($key));
        if ($arquivo) {
            return unserialize(trim($arquivo));
        }
        return null;
    }

    public function delete(string $key)
    {
        if (file_exists($this->file($key))) {
            return unlink($this->file($key));
        }
        return null;
    }

    public function save(string $key, $value, int $lifetime)
    {
        $fp = fopen($this->file($key), "wb");
        fwrite($fp, serialize($value));
        fclose($fp);
        return true;
    }
}