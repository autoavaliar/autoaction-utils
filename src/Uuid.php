<?php

declare(strict_types=1);

namespace AutoAction\Utils;

/**
 * Uuid
 *
 * @package AutoAction\Utils
 * @date    24/07/2019
 *
 * @author  Lucas Trindade <lucas.silva@autoavaliar.com.br>
 */
class Uuid
{
    /**
     * Devolve um UUID versão 4
     *
     * @return string
     */
    public static function uuid4(): string
    {
        $data = random_bytes(16);
        assert(strlen($data) == 16);

        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
}