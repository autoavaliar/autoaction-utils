<?php

declare(strict_types=1);

namespace AutoAction\Utils\Vbn;

use AutoAction\Utils\Vbn\Bucket\BucketInterface;
use AutoAction\Utils\Vbn\Exceptions\VbnException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Classe que faz a comunicação e retorna o link do arquivo.
 *
 * @package AutoAction\Utils
 * @date    09/08/2021
 *
 * @author  Daniel Lemes <devel_15@autoavaliar.com.br>
 */
class Vbn
{

    /**
     * @var VbnConfig
     */
    private $config;
    private $bucket;
    private $filePath;
    private $client;

    /**
     * @param VbnConfig $config
     */
    public function __construct(VbnConfig $config)
    {
        $this->config = $config;
    }

    public function setBucket(BucketInterface $bucket)
    {
        $this->bucket = $bucket->getName();
        $this->filePath = $bucket->getFile();
    }

    /**
     * @throws GuzzleException
     * @throws VbnException
     */
    public function getFile(): string
    {
        $client = $this->getClient();
        $request = $client->request(
            $this->config->getMethod(),
            $this->config->getHost(),
            [
                'headers' => [
                    'token' => $this->config->getToken(),
                ],
                'allow_redirects' => false,
                'query' => [
                    'bucketName' => $this->getBucket(),
                    'filename' => $this->getFilePath()
                ]
            ]
        );

        if ($request->getStatusCode() == 302) {
            return $request->getHeader('location')[0];
        }

        throw new VbnException('Erro não previsto', E_CORE_ERROR);
    }

    private function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(Client $client)
    {
        $this->client = $client;
    }

    public function getBucket(): string
    {
        return $this->bucket;
    }

    public function getFilePath(): string
    {
        return $this->filePath;
    }
}