<?php

declare(strict_types=1);

namespace AutoAction\Utils\Vbn\Bucket;

interface BucketInterface
{

    public function getName();

    public function getFile();

}