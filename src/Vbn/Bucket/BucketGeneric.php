<?php

declare(strict_types=1);

namespace AutoAction\Utils\Vbn\Bucket;


/**
 * Classe de exemplo de como implementar o bucket interface
 *
 * @package AutoAction\Utils
 * @date    09/08/2021
 *
 * @author  Daniel Lemes <devel_15@autoavaliar.com.br>
 */
class BucketGeneric implements BucketInterface
{
    private $file;
    private $name;

    public function __construct(string $name = '', string $file = '')
    {
        $this->setName($name);
        $this->setFile($file);
    }

    public function setName(string $name)
    {
        if (!empty($name)) {
            $this->name = trim($name);
        }
    }

    public function setFile(string $file)
    {
        if (!empty($file)) {
            $this->file = trim($file);
        }
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getFile(): string
    {
        return $this->file;
    }
}