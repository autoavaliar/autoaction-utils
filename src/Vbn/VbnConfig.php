<?php

namespace AutoAction\Utils\Vbn;

use AutoAction\Utils\Vbn\Exceptions\VbnConfigException;

/**
 * Classe de configuração do VBN, recebe host, token e método.
 *
 * @package AutoAction\Utils
 * @date    09/08/2021
 *
 * @author  Daniel Lemes <devel_15@autoavaliar.com.br>
 */
class VbnConfig
{
    private $host;
    private $method;
    private $token;
    private $methodValues
        = [
            'GET',
            'POST'
        ];

    /**
     * @throws VbnConfigException
     */
    public function __construct($host, $token, $method = 'GET')
    {
        if (!filter_var($host, FILTER_VALIDATE_URL)) {
            throw new VbnConfigException("Host URL is invalid");
        }
        if (!in_array($method, $this->methodValues)) {
            throw new VbnConfigException("The method used is invalid");
        }

        $this->host = $host;
        $this->method = $method;
        $this->token = $token;
    }

    public function getHost()
    {
        return $this->host;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function getToken()
    {
        return $this->token;
    }
}