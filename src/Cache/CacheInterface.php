<?php

declare(strict_types=1);

namespace AutoAction\Utils\Cache;

/**
 * Gerenciamento de cache
 *
 * @package AutoAction\Utils
 * @date    29/04/2020 14:01
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
interface CacheInterface
{
    /**
     * Caso o cache precise de autenticação, informar a senha aqui.
     * @param string $password Senha de acesso ao Redis
     * @return bool
     */
    public function auth($password): bool;

    /**
     * Efetua a conexão com o Redis
     * @param string      $host    Nome do servidor
     * @param int         $port    Porta de acesso
     * @param int         $timeout Tempo padrão de expiração da chave
     * @param null|string $reserved
     * @param int         $retry_interval
     * @param float       $read_timeout
     */
    public function connect(
        $host,
        $port = 6379,
        $timeout = 1800,
        $reserved = '',
        $retry_interval = 0,
        $read_timeout = 0.0
    );

    /**
     * Salvar um registro no redis
     * @param string $keyName  Nome da chave
     * @param mixed  $value    Valor a ser salvo
     * @param bool   $lifetime Tempo de expiração do registro
     * @return bool
     */
    public function save($keyName, $value, $lifetime = false): bool;

    /**
     * Recupera um registro no Redis a partir de uma chave
     * @param string $keyName
     * @return bool|mixed|string
     */
    public function get($keyName);

    /**
     * Exclui uma chave do Redis
     * @param string $keyName
     * @return int
     */
    public function delete($keyName);

    /**
     * Verifica se uma determinada chave está disponível
     * @param string $keyName
     * @return int
     */
    public function exists($keyName);

    /**
     * Lista de chaves baseado em um prefix
     * @param string $prefix Exemplo: my-key-* (Busca todas as chaves que comecem com "my-key-") REGEX pode ser utilizado aqui
     * @return array
     */
    public function keys($prefix);

    /**
     * Retorna a lista de chaves baseado na string informada
     * É uma simplificação do método ->keys
     * @param string $query texto que deseja buscar nas chaves
     * @return array
     */
    public function queryKeys($query);

    /**
     * Registra o tempo de expiração da informação
     * @param string $key
     * @param int    $ttl
     * @return bool
     */
    public function setTimeout($key, $ttl);
}