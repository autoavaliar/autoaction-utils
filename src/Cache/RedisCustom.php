<?php

declare(strict_types=1);

namespace AutoAction\Utils\Cache;

use Redis;

/**
 * Simplificação da utilização do \Redis
 *
 * @package AutoAction\Utils
 * @date    29/04/2020 12:30
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class RedisCustom implements CacheInterface
{
    /**
     * Instancia do Redis padrão do PHP
     * @var Redis
     */
    private $redis;

    /**
     * Inicializa o Redis()
     */
    public function __construct()
    {
        $this->redis = new Redis();
    }

    /**
     * Recupera a instancia padrão do Redis
     * @return Redis
     */
    public function getRedisBase(): Redis
    {
        return $this->redis;
    }

    /**
     * Caso o Redis precise de autenticação, informar a senha aqui.
     * @param string $password Senha de acesso ao Redis
     * @return bool
     */
    public function auth($password): bool
    {
        if (!$password) {
            return false;
        }

        return $this->redis->auth($password);
    }

    /**
     * Efetua a conexão com o Redis
     * @param string $host Nome do servidor
     * @param int $port Porta de acesso
     * @param int $timeout Tempo padrão de expiração da chave
     * @param null|string $reserved
     * @param int $retry_interval
     * @param float $read_timeout
     */
    public function connect(
        $host,
        $port = 6379,
        $timeout = 1800,
        $reserved = '',
        $retry_interval = 0,
        $read_timeout = 0.0
    ) {
        $this->redis->connect($host, $port, $timeout, $reserved, $retry_interval, $read_timeout);
    }

    /**
     * Salvar um registro no redis
     * @param string $keyName Nome da chave
     * @param mixed $value Valor a ser salvo
     * @param bool $lifetime Tempo de expiração do registro
     * @return bool
     */
    public function save($keyName, $value, $lifetime = false):bool
    {
        $lifetime = $lifetime ? $lifetime : 300; // tempo padrão: 5 minutos
        return $this->redis->setex($keyName, $lifetime, serialize($value));
    }

    /**
     * Recupera um registro no Redis a partir de uma chave
     * @param string $keyName
     * @return bool|mixed|string
     */
    public function get($keyName)
    {
        $value = $this->redis->get($keyName);

        if(!$value) return null;

        return unserialize($value);
    }

    /**
     * Exclui uma chave do Redis
     * @param string $keyName
     * @return int
     */
    public function delete($keyName)
    {
        return $this->redis->del($keyName);
    }

    /**
     * Verifica se uma determinada chave está disponível
     * @param string $keyName
     * @return int
     */
    public function exists($keyName)
    {
        return $this->redis->exists($keyName);
    }

    /**
     * Lista de chaves baseado em um prefix
     * @param string $prefix Exemplo: my-key-* (Busca todas as chaves que comecem com "my-key-") REGEX pode ser utilizado aqui
     * @return array
     */
    public function keys($prefix)
    {
        return $this->redis->keys($prefix);
    }

    /**
     * Retorna a lista de chaves baseado na string informada
     * É uma simplificação do método ->keys
     * @param string $query texto que deseja buscar nas chaves
     * @return array
     */
    public function queryKeys($query)
    {
        return $this->redis->keys('*' . $query . '*');
    }

    /**
     * Registra o tempo de expiração da informação
     * @param string $key
     * @param int $ttl
     * @return bool
     */
    public function setTimeout($key, $ttl)
    {
        return $this->redis->expire($key, $ttl);
    }
}