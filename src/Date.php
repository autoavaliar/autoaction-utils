<?php
declare(strict_types=1);

namespace AutoAction\Utils;

use DateTimeZone;

/**
 * Classe para trabalhar com datas
 * @author Murilo Pucci <murilo.pucci@autoavaliar.com.br>
 * @date 16/08/2019
 * @package Mercury\Common\Util
 */
class Date
{
    /**
     * Cria um objeto de Datetime com a hora atual com milisegundos
     * no formato: Y-m-d H:i:s.u
     * @param DateTimeZone|null $timezone
     * @return \DateTime
     * @throws \Exception
     */
    public static function nowWithMiliseconds(DateTimeZone $timezone = null) : \DateTime
    {
        $curDate = date("Y-m-d H:i:s");
        list($curDateMiliseconds) = explode(' ', microtime());
        $curDateMilisecondsFormatted = substr(str_replace("0.", ".", $curDateMiliseconds), 0, 7);
        return new \Datetime($curDate . $curDateMilisecondsFormatted, $timezone);
    }
}